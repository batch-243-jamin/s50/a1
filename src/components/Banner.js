import Button from "react-bootstrap/Button";
/* Bootstrap Grid System*/
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import {Link} from "react-router-dom"


export default function Banner(){

	return(
		<Row className = "justify-content-center">
			<Col className = "text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>

				<Button as = {Link} to = "/courses" variant = "primary">Enroll Now!</Button>
			</Col>
		</Row>
		)
}