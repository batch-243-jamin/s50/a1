// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){

	return(
		<Row className = "my-3">
	{/*This is the first card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        <h2>Learn From Home</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et metus mauris. Sed vel scelerisque nibh. Pellentesque sodales porta est. Praesent a dolor dignissim, placerat felis eget, lobortis ex. Fusce sodales massa volutpat est lobortis, eget imperdiet ex sollicitudin. Quisque tincidunt ex nec leo consectetur convallis. Morbi tristique elit lacus, ut luctus augue tincidunt sit amet. Duis euismod tincidunt massa, ac varius ante tincidunt tincidunt. Mauris volutpat pellentesque felis eu aliquam. Aenean molestie, justo cursus cursus congue, nisl tellus ornare ligula, eu blandit neque urna non mauris. Duis lacus felis, semper convallis varius non, congue id justo. Integer ornare gravida ex, non volutpat dolor pulvinar eget. Donec egestas facilisis sem ut pulvinar. Mauris vitae nibh massa. Nulla pretium suscipit aliquam. Suspendisse ullamcorper nisl vitae tortor commodo scelerisque.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*This is the 2nd card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        <h2>Study Now, Pay Later</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et metus mauris. Sed vel scelerisque nibh. Pellentesque sodales porta est. Praesent a dolor dignissim, placerat felis eget, lobortis ex. Fusce sodales massa volutpat est lobortis, eget imperdiet ex sollicitudin. Quisque tincidunt ex nec leo consectetur convallis. Morbi tristique elit lacus, ut luctus augue tincidunt sit amet. Duis euismod tincidunt massa, ac varius ante tincidunt tincidunt. Mauris volutpat pellentesque felis eu aliquam. Aenean molestie, justo cursus cursus congue, nisl tellus ornare ligula, eu blandit neque urna non mauris. Duis lacus felis, semper convallis varius non, congue id justo. Integer ornare gravida ex, non volutpat dolor pulvinar eget. Donec egestas facilisis sem ut pulvinar. Mauris vitae nibh massa. Nulla pretium suscipit aliquam. Suspendisse ullamcorper nisl vitae tortor commodo scelerisque.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*This is the 3rd card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        <h2>Be part of our community</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et metus mauris. Sed vel scelerisque nibh. Pellentesque sodales porta est. Praesent a dolor dignissim, placerat felis eget, lobortis ex. Fusce sodales massa volutpat est lobortis, eget imperdiet ex sollicitudin. Quisque tincidunt ex nec leo consectetur convallis. Morbi tristique elit lacus, ut luctus augue tincidunt sit amet. Duis euismod tincidunt massa, ac varius ante tincidunt tincidunt. Mauris volutpat pellentesque felis eu aliquam. Aenean molestie, justo cursus cursus congue, nisl tellus ornare ligula, eu blandit neque urna non mauris. Duis lacus felis, semper convallis varius non, congue id justo. Integer ornare gravida ex, non volutpat dolor pulvinar eget. Donec egestas facilisis sem ut pulvinar. Mauris vitae nibh massa. Nulla pretium suscipit aliquam. Suspendisse ullamcorper nisl vitae tortor commodo scelerisque.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		)
}