import {useState, useEffect, useContext} from 'react';
import {Row, Col, Card} from 'react-bootstrap'
import Button from "react-bootstrap/Button";
import UserContext from '../UserContext';
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
	// console.log(props);

	// destructuring the courseProp that were passed from the Course.js

	const {_id, name, description, price, slots} = courseProp;

	// Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
	// States are used to keep track information related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)
		// getter variable, setter function

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots);

	const [isAvailable, setIsAvailable] = useState(true);

	const {user} = useContext(UserContext);

	// Add an "useEffect" hook to have "CourseCard" component do perform a certain task every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(()=>{
		if(slotsAvailable === 0){

		setIsAvailable(false);

		}
	 	console.log(isAvailable);

	}, [slotsAvailable]);

	// console.log(enrollees);

	// setEnrollees(1);

	function enroll(){
		if(slotsAvailable === 1){
			alert("Congratulations, you were able to before the cut!");
		}
		setEnrollees(enrollees+1)
		setSlotsAvailable(slotsAvailable-1);
	}


	// since enrollees is declared as a constant variable, directly reassigning the value is not allowed or will cause an error

	// enrollees = 1;
	// console.log(enrollees);

	return(
		<Row>
		<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
		<Card className = "h-100 p-3">
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Text className="h6">Description:</Card.Text>
		        <Card.Text>
		          {description}
		        </Card.Text>
		        <Card.Text className = "h6">Price:</Card.Text>
		        <Card.Text>
		          P{price}
		        </Card.Text>
		        <Card.Text className = "h6">Enrollees:</Card.Text>
		        <Card.Text>
		          	{enrollees}
		        </Card.Text>
		        <Card.Text className = "h6">Slots available:</Card.Text>
		        <Card.Text>
		          {slotsAvailable} slots
		        </Card.Text>
		       {
		       	(user !== null) ?
		       	 <Button variant="primary" as = {Link} to = {`/courses/${_id}`} disabled = {!isAvailable}>Details</Button>
		       	 :
		       	  <Button as = {Link} to = "/login" variant="primary"  disabled = {!isAvailable}>Enroll</Button>
		       }
		      </Card.Body>
		    </Card>
		</Col>
		</Row>
		)
}