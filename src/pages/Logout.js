import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){
	// Mini-activity:
		// Once na clinick si logout it should automatically rerender the AppNavbar.
	const {user, setUser, unSetUser} = useContext(UserContext);
	console.log(user);
	unSetUser();

	useEffect(()=>{
		setUser({id: null, isAdmin: false});
	}, [])

	Swal.fire({
		title: "Welcome back to the outside World!",
		icon: "info",
		text: "Babalik ka rin"
	})

		return(
			<Navigate to = '/login'/>
		)
}