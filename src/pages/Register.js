import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
// import UserContext from '../UserContext';
// import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Register = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobile, setMobile] = useState("");
    const [passwordOne, setPasswordOne] = useState("");
    const [passwordTwo, setPasswordTwo] = useState("");
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();
    
    // const { user, setUser } = useContext(UserContext);

    useEffect(() => {
        firstName !== "" && lastName !== "" && email !== "" && mobile !== "" && passwordOne !== "" && passwordTwo !== "" && passwordOne === passwordTwo
        ? setIsActive(true)
        : setIsActive(false)

    }, [firstName, lastName, email, mobile, passwordOne, passwordTwo]);

    const submitRegister = e => {
        e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                mobileNo: mobile,
                password: passwordOne,
                passwordTwo: passwordTwo,
            })

        }).then(response => response.json())
        .then(data =>{
            console.log(data);

            if(data.emailExists) {
                Swal.fire({
                    title: "Email Already Exists",
                    icon: "error",
                    text: "Please use another email"
                })

                setPasswordOne('');
                setPasswordTwo('');
            } 

            else if (data.mobileLength === false) {
                Swal.fire({
                    title: "Invalid Mobile Number",
                    icon: "error",
                    text: "Mobile number must be at least 11 digits to be valid."
                })

                setPasswordOne('');
                setPasswordTwo('');
            }

            else {
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "You may now login to our site!"
                })

                navigate('/login');
            }
        })
    }

    return (
        <Container>
            <Row className='w-100'>
                <Col className='col-md-4 col-8 offset-md-4 offset-2'>
                    <Form className='bg-secondary p-3' onSubmit={submitRegister}>
                        <Form.Group className="mb-3" controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                value={firstName}
                                type="text"
                                placeholder="Enter first name"
                                required
                                onChange={e => setFirstName(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                value={lastName}
                                type="text"
                                placeholder="Enter last name"
                                required
                                onChange={e => setLastName(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                value={email}
                                type="email"
                                placeholder="Enter email"
                                required
                                onChange={e => setEmail(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="mobile">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                value={mobile}
                                type="number"
                                placeholder="Enter mobile number"
                                required
                                onChange={e => setMobile(e.target.value)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordOne">
                            <Form.Label>Enter your desired password</Form.Label>
                            <Form.Control
                                value={passwordOne}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPasswordOne(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordTwo">
                            <Form.Label>Verify password</Form.Label>
                            <Form.Control
                                value={passwordTwo}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPasswordTwo(e.target.value)}/>
                        </Form.Group>
                        
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Register
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
        
    )
}

export default Register
