import {Button, Form, Row, Col, Container} from 'react-bootstrap';
// Bootstrap grid system
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import userContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// const [user, setUser] = useState(null);
	// Allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(userContext);


	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true);
			}
		else{
			setIsActive(false);
		}	
	}, [email, password,]);

	function loginUser(event){
		event.preventDefault()

		// alert(`Thank you for logging in!`)

		// Set the email of the authenticated user in the local Storage
		// Syntax:
			// localStorage.setItem('propertyName',value)

		// Storing information in the local storage will make the data persistent even as the page is refreshed unlike with the use of states where informations is reset when refreshing the page
		// localStorage.setItem('email', email);

		// Set the global user state to have properties obtained from the local storage

		// though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and re-render it to avoid refreshing the page upon user login and logout
		// setUser(localStorage.getItem("email"));

		// setEmail('');
		// setPassword('');

		// Process wherein it will fetch a request to the corresponding API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of json
		// The fetch request will communicate wil our backend application providing it with a stringified JSON

		// Syntax:
			// fetch ('url, {options: method, headers body}')
			// .then(response => res.json())
			// .then(data => {data process})

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {

			method: "POST",
			headers: {
				'Content-Type': 'application/json'},
				body: JSON.stringify({
					email: email,
					password: password
				})
			
		}).then(response => response.json())
		.then(data => {
			// It is good to practice to always use or print out the result of our fetch request to ensure the correct information is received in our frontend application
			console.log(data);

			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				})
			}else{
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Check your login details and try again!"
				})
				setPassword('');
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}})
				.then(response => response.json())
				.then(data => {
					console.log(data);
					setUser({id: data._id, isAdmin: data.isAdmin})
			})
		}


	}

	return(
		
		(user.id !== null || undefined) ?
			<Navigate to = "/"/>
		:
		<Container>
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2 mt-2">
					<Form onSubmit = {loginUser} className = "bg-secondary p-3">
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control
					         type="email" 
					         placeholder="Enter email" 
					         value = {email}
					         onChange = {event => setEmail(event.target.value
					         	)}
					         required/>
					      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Enter Your Password</Form.Label>
				        <Form.Control 
				        type="password" 
				        placeholder="Password"
				        onChange = {event => setPassword(event.target.value)}
				        value = {password}
				        required/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicCheckbox">
				        <Form.Check
				         type="checkbox" 
				         label="Keep me logged in" />
				      </Form.Group>
				      <Button variant="primary" type="submit" disabled = {!isActive}>
				        Login
				      </Button>
				    </Form>
		    	</Col>
			</Row>	
		</Container>
		)
}