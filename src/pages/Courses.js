import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
// import courseData from '../data/courses.js';

export default function Courses(){
	const [courses, setCourses] = useState([]);
	// console.log(courseData);

	// Syntax:
		// localStorage.getItem("propertyName");

	// const local = localStorage.getItem("email");
	// console.log(local);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/allActiveCourses`)
		.then(response => response.json())
		.then(data => {
			console.log(data);

			// Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" component
			setCourses(data.map(course => {

		return(
			<CourseCard key = {course._id} courseProp = {course}/>)
		}))
	})

	}, []);

	return(
		<Fragment>
		{courses}
		</Fragment>
		)
}